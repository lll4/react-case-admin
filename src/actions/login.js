import actionType from './actionType';
import { postLogin } from '../requests'

// 在登录的时候显示登录中，修改isLoading,表单里面的disabled根据这个属性来决定
export const showLoading = () => {
  return {
    type: actionType.SHOW_LOADING
  }
}

// 登录成功后修改isLoading的值
export const closeLoading = () => {
  return {
    type: actionType.CLOSE_LOADING
  }
}

// 登录成功后，设置登录状态
export const loginSuccess = (payload) => {
  return {
    type: actionType.LOGIN_SUCCESS,
    payload
  }
}
 const checkingTokenSuccess = () => {
  return {
    type: actionType.CHECKING_TOKEN_SUCCESS
  }
}
export const checkingToken = () => {
  return dispatch => {
    //模拟一次请求
    setTimeout(() => {
      dispatch(checkingTokenSuccess())
    // 如果token有效
    const isOk = window.localStorage.getItem('token')//真实的请求这个值会返回
    const permission = JSON.parse(window.localStorage.getItem('permission'))
    if (isOk) {
      dispatch(loginSuccess({
        isLogin: true,
        permission
      }))
    }
    }, 2000)
  }
}

export const doLogin = (params) => {
  // redux异步处理
  return dispatch => {
    // 登录的时候先显示在登录中
    dispatch(showLoading());
    // 请求登录接口
    postLogin(params).then(resp => {
      console.log(resp)
      // 如果返回的参数含有authToken
      if (resp.token) {
        // 存到localStorage
        window.localStorage.setItem("token", resp.token);
        window.localStorage.setItem("username", resp.username);
        window.localStorage.setItem("permission", JSON.stringify(resp.permission));
        // dispatch登录成功这个函数，修改state里面的值，登录状态改为true,并且把后端返回的用户名放入state
        dispatch(loginSuccess({
          username: resp.username,
          permission: resp.permission,
          isLogin: true
        }))
        // 关闭加载中状态
        dispatch(closeLoading());
      }
    })
  }
}
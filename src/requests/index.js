import axios from 'axios';

const ajax = axios.create({
  baseURL: 'http://rap2api.taobao.org/app/mock/122773'
})

axios.interceptors.request.use(config => {
  // config.headers.authToken = 'asdfasdasdfasdfasdfasd'
  config.data = {
    ...config.data,
    authToken: window.localStorage.getItem('token')//这个token应该是从localStorage里取
  }
  return config;
})

axios.interceptors.response.use(resp => {
  if (resp.data.code === 200) {
    return resp.data.data
  } else {
    return {
      errorMsg: resp.data.msg || '出错了！！！'
    }
  }
})

// // 前端分页接口
export const postUserList = () => {
  return ajax.post('/api/v1/userList')
}

// 后端分页接口
export const postUserListPagination = (params) => {
  return ajax.post('/api/v1/userListPagination', params)
}

// 删除用户
export const postDeleteUser = (id) => {
  return ajax.post(`/api/v1/user/delete/${id}`)
}

// 人员查看列表接口
export const postUsersViewList = (params) => {
  return axios.post("http://rap2api.taobao.org/app/mock/123608/api/v1/users_view", params)
}

// 病事假管理
export const postLeavePagination = (params) => {
  return axios.post('http://rap2api.taobao.org/app/mock/122883/api/v1/leave', params)
}

// // 删除用户
export const postDeleteLeave = (id) => {
  return axios.post(`http://rap2api.taobao.org/app/mock/122883/api/v1/leave/delete/${id}`)
}

// 搜索请求
export const postSearch = (params) => {
  return axios.post("http://rap2api.taobao.org/app/mock/123608/api/v1/users_search", params)
}

// 个人信息请求接口
export const postUserInfo = (id) => {
  return axios.post(`http://rap2api.taobao.org/app/mock/123608/api/v1/user_view/user_info${id}`)
}

// 登录请求接口
export const postLogin = (params) => {
  return axios.post("http://rap2api.taobao.org/app/mock/122883/api/v1/login", params)
}

//个人信息接口
export const person = (params) => {
  return axios.post("http://rap2api.taobao.org/app/mock/122883/api/v1/person", params)
}

//离退人员接口
export const retireman = (params) => {
  return axios.post("http://rap2api.taobao.org/app/mock/122883/api/v1/retireman", params)
}

//离退人员详细接口
export const retiremancheck = (id) => {
  return axios.post(`http://rap2api.taobao.org/app/mock/122883/api/v1/retireman/lotus/${id}`)
}
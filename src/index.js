import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
// Provider是react-redux里提供的一个组件，这个组件，需要一个参数store
import { Provider } from "react-redux"
import App from './App';
import store from "./store"
import './index.less';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('root')
);


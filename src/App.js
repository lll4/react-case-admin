import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import routes from './routes'
import { AppFrame } from './components'
import { Login } from './pages';
import { connect } from "react-redux"
import { Spin } from "antd"

import {
  checkingToken
} from './actions/login'

const mapState = (state) => {
  return {
    isLogin: state.users.isLogin,
    isCheckingToken: state.users.isCheckingToken,
    permission: state.users.permission
  }
}
@connect(mapState, { checkingToken })
class App extends Component {
  componentDidMount() {
    this.props.checkingToken()
  }

  // shouldComponentUpdate(nextProps) {
  //   console.log(nextProps)
  //   return nextProps.route.permission.length
  //   // return false
  // }

  render () {
    const {
      isCheckingToken,
      // permission
    } = this.props;
    // 解决没有这个权限输入路径搜索依然显示网页
  //  const pRoutes = routes.reduce((result, route) => {
  //    if (!route.permission || permission.includes(route.permission)) {
  //     result.push(route)
  //    }
  //    return result
  //  }, [])
  //  console.log(pRoutes)
    return isCheckingToken
    ? <Spin></Spin>
    : (
      
      <Switch>
        <Route path="/login" component={Login}></Route>
        <AppFrame>
          <Switch>
            {
              // 渲染路由
              routes.map(route => {
                let result = [];
                if (route.children && route.children.length) {
                  result = route.children.map(childRoute => {
                    return (
                      <Route
                        key={childRoute.path}
                        path={childRoute.path}
                        render={
                          (Routeprops) => {
                            // console.log(route)
                            const {
                              isLogin,
                              // permission
                            } = this.props
                            // 把所有路由组件赋值给Com,然后判断isLogin是不是为true，如果是，就可以渲染这些组件，登录成功，如果不是，就继续回到Login界面
                            const ChildCom = childRoute.component
                            
                            if (!isLogin) {
                              return <Redirect to={{
                                pathname: "/login",
                                // 传state
                                state: {
                                  from: route.path
                                }
                              }} />
                            } else {
                              return <ChildCom {...Routeprops} />
                            }
                          }
                        }
                        exact={route.isExact}
                      />
                    )
                  })
                }
                result.push(<Route
                  key={route.path}
                  path={route.path}
                  render={
                    (Routeprops) => {
                      const {
                        isLogin
                      } = this.props
                      // 把所有路由组件赋值给Com,然后判断isLogin是不是为true，如果是，就可以渲染这些组件，登录成功，如果不是，就继续回到Login界面
                      const Com = route.component
                      return isLogin ? <Com {...Routeprops} /> : <Redirect to={{
                        pathname: "/login",
                        // 传state
                        state: {
                          from: route.path
                        }
                      }} />
                    }
                  }
                  exact={route.isExact}
                />)
                return result
              })
            }
            <Redirect exact to={routes[0].path} from='/' />
            <Redirect to='/404' />
          </Switch>
        </AppFrame>
      </Switch>
    );
  }
}

export default App;

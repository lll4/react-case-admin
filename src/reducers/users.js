import actionType from '../actions/actionType'
// isLogin: false,不管到那个页面都会返回到登录页
// window.localStorage.getItem('token')登录成功后使刷新不返回登录页
const initState = {
  isLogin: window.localStorage.getItem('token'),
  isLoading: false,
  permission: [],
  isCheckingToken: true,
  username: ''
}

export default (state = initState, action) => {
  // console.log(action)
  switch (action.type) {
    case actionType.SHOW_LOADING:
      return {
        ...state,
        isLoading: true
      }
    case actionType.CLOSE_LOADING:
      return {
        ...state,
        isLoading: false
      }
    case actionType.LOGIN_SUCCESS:
      // console.log(action)
      return {
        ...state,
        ...action.payload
      }
    case actionType.CHECKING_TOKEN_SUCCESS:
      return {
        ...state,
        isCheckingToken: false
      }
    default:
      return state
  }
}
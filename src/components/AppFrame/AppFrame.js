import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  Layout, Menu, Icon, Button
} from 'antd';
import { connect } from 'react-redux';
import './AppFrame.less';
import routes from '@/routes';
const navRoutes = routes.filter(route => route.isNav === true);
console.log(navRoutes[1])
const vRoutes = routes.filter(route => route.children);
// console.log(vRoutes[0].children[0].permission)
const na = vRoutes.map(function(v){return v.children;});
// var nas = na.map(function(v){return v;});
console.log(na[0])

console.log(na[0][0].permission)

console.log(vRoutes)

const { Header, Content, Sider } = Layout;
const { SubMenu } = Menu;

// 装饰器模式的写法，需要配置babel插件: babel-plugin-transform-decorators-legacy
@connect((state) => {
  return {
    permission: state.users.permission
  }
})
@withRouter
export default class AppFrame extends Component {
  static getDerivedStateFromProps (props) {

    // 侧边栏高亮选中逻辑处理
    const selectedKey = `/${props.location.pathname.split('/').reverse()[0]}` || navRoutes[0].path;
    //console.log(selectedKey)
    // submenu展开逻辑处理
    let openKey = []
    for (let menuObj of navRoutes) {
      if (menuObj.children) {
        for (let menuList of menuObj.children) {
          if (menuList.path === selectedKey) {
            openKey = [menuObj.path]
            break
          }
        }
      }
    }
    return {
      selectedKey,
      openKey
    }
  }
  constructor() {
    super();
    this.state = {
      selectedKey: navRoutes[0].path,
      openKey: []
    }
  }
  // 点击左边侧边栏，进入所点击的页面
  handleMenuItemClick = ({ key }) => {
    this.props.history.push(key)
  }

  handleMenuItemExit = () => {
    window.localStorage.removeItem('token')
    window.localStorage.removeItem('username')
    window.localStorage.removeItem('permission')
    window.location.reload()
  }
  render () {
    const navRoutesWithPermission = navRoutes.filter(route => route.permission.includes(this.props.permission))
    const childrenRoutesWithPermission = na[0].filter(route => route.permission.includes(this.props.permission))
    return (
      <Layout className="r-layout">
        <Header className="header r-header">
          <div className="logo">案事件管理子系统</div>
          <div className="btn-login">
          {window.localStorage.getItem('username')}
          <Button className="tc" onClick={this.handleMenuItemExit}>退出</Button>
          </div>
        </Header>
        <Layout>
          <Sider width={200} style={{
            backgroundColor: '#fff',
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0,
            paddingTop: 64
          }}
          >
            <Menu
              mode="inline"
              onClick={this.handleMenuItemClick}
              // 侧边栏高亮选中
              selectedKeys={[this.state.selectedKey]}
              style={{ height: '100%', borderRight: 0 }}
              // submenu默认展开
              defaultOpenKeys={this.state.openKey}
            >
              {
                navRoutesWithPermission.map(route => {
                  if (route.children && route.children.length) {
                    return (
                      <SubMenu
                        key={route.path}
                        title={<span><Icon type={route.iconType} />{route.title}</span>}
                      >
                        {
                          childrenRoutesWithPermission.map(menu => {
                            return (<Menu.Item
                              key={menu.path}
                            >
                              <Icon type={menu.iconType} style={{ fontSize: 16 }}></Icon>
                              {menu.title}

                            </Menu.Item>
                            )
                          })
                        }
                      </SubMenu>
                    )
                  }
                  return (
                    <Menu.Item
                      key={route.path}
                    >
                      {route.title}
                    </Menu.Item>
                  )
                })
              }

            </Menu>
          </Sider>
          <Layout style={{ padding: '24px', marginLeft: 200 }}>
            <Content style={{
              background: '#fff',
              padding: 24,
              margin: 0,
              minHeight: 280,
              marginTop: 64
            }}
            >
              {this.props.children}
            </Content>
          </Layout>
        </Layout>
      </Layout>
    )
  }
}

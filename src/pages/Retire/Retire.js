import React, { Component } from 'react'
import {
  Form,
  Input,
  Button,
  Row,
  Col,
  Select,
  DatePicker
} from 'antd';
import './retire.less'

const { Option } = Select;
const formItemLayout = {
  labelCol: { span: 4},
  wrapperCol: { span: 12},
}

@Form.create()
export default class UserEdit extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        alert('离退申请提交成功')
      }
    });
  }

  handleCancel = () => {
    this.props.history.push(`/retire`)
  }

  render () {
    const config = {
      rules: [{ type: 'object', required: true, message: '请选择时间' }],
    };
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="wrap1">
        <h1 style={{ 'marginLeft': '400px' }}>离退申请</h1>
        <Row gutter={36} justify="center" type="flex">
          {/* justify="center" type="flex"居中 */}
          <Col span={24} start={13}>
            <Form onSubmit={this.handleSubmit} className="login-form">
              <Form.Item
                {...formItemLayout}
                label="编号"
              >
                {
                  getFieldDecorator(
                    'num',
                    {
                      rules: [{
                        required: true,
                        message: '请输入编号',
                      }, {
                        validator: (rule, value, cb) => {
                          var regex = /^[0-9]{5}$/;
                          if (value) {
                            if (regex.test(value)) {
                              cb();
                            } else {
                              cb('请输入五位数字');
                            }
                          } else {
                            cb()
                          }
                        }
                      }],
                    }
                  )(
                    <Input
                      placeholder="请输入编号"
                    />
                  )
                }
              </Form.Item>

              <Form.Item
                {...formItemLayout}
                label="姓名"
              >
                {
                  getFieldDecorator(
                    'name',
                    {
                      // 判断条件
                      // whitespace: true,检查空格
                      rules: [{
                        required: true,
                        message: '请输入中文名'
                      }, {
                        validator: (rule, value, cb) => {
                          var regex = /^[\u4E00-\u9FA5]{2,4}$/;
                          if (value) {
                            if (regex.test(value)) {
                              cb();
                            } else {
                              cb('请输入中文名');
                            }
                          } else {
                            cb()
                          }
                        }
                      }],
                    }
                  )(
                    <Input
                      placeholder="请输入姓名"
                    />
                  )
                }
              </Form.Item>

              <Form.Item
                {...formItemLayout}
                label="年龄"
              >
                {
                  getFieldDecorator(
                    'age',
                    {
                      // 判断条件
                      // whitespace: true,检查空格
                      rules: [{
                        required: true,
                        message: '请输入年龄',
                      }, {
                        validator: (rule, value, cb) => {
                          var regex = /^(1[9]|[2-8][0-9]|60)$/;
                          if (value) {
                            if (regex.test(value)) {
                              cb();
                            } else {
                              cb('请输入年龄在19到60岁之间');
                            }
                          } else {
                            cb()
                          }
                        }
                      }],
                    }
                  )(
                    <Input
                      placeholder="请输入年龄"
                    />
                  )
                }
              </Form.Item>

              <Form.Item
                label="职位"
                {...formItemLayout}
              >
                {
                  getFieldDecorator('position', {
                    rules: [{
                      required: true,
                      message: '请选择职位'
                    }],
                  }
                  )(
                    <Select
                      placeholder="请选择职位"
                      onChange={this.handleSelectChange}
                    >
                      <Option value="科长">科长</Option>
                      <Option value="副科长">副科长</Option>
                      <Option value="科员">科员</Option>
                      <Option value="队员">队员</Option>
                      <Option value="处级领导">处级领导</Option>
                      <Option value="治安科长">治安科长</Option>
                    </Select>
                  )}
              </Form.Item>
              <Form.Item
         {...formItemLayout}
         label="地址"
        >
          {
            getFieldDecorator(
              'address', 
            {
              rules: [{ 
                required: true, 
                message: '请输入地址' 
              }],
            }
          )(
            <Input
              placeholder="请输入地址"
            />
          )
        }
        </Form.Item>

        <Form.Item
         {...formItemLayout}
         label="电话号码"
        >
          {
            getFieldDecorator(
              'phone', 
            {
              // 判断条件
              // whitespace: true,检查空格
              rules: [{
                required: true,
                message: '请输入电话号码',
              }, {
                validator: (rule, value, cb) => {
                  var regex = /^1[34578]\d{9}$/; 
                if (value) {
                  if (regex.test(value)) {
                    cb();
                  } else { 
                    cb('请输入电话号码');
                  }
                } else {
                  cb()
                }
               }
              }],
            }
          )(
            <Input
              placeholder="请输入电话号码"
            />
          )
        }
        </Form.Item>

        <Form.Item
          {...formItemLayout}
          label="离退时间"
          className="time"
        >
          {getFieldDecorator('date-time-picker', config)(
            <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
          )}
        </Form.Item>

        <Form.Item
         {...formItemLayout}
         label="离退原因"
        >
          {
            getFieldDecorator(
              'discription', 
            {
              rules: [{ 
                required: true, 
                message: '请输入离退原因' 
              }],
            }
          )(
            <textarea className="TextArea"></textarea>
          )
        }
        </Form.Item>

              <Form.Item className="btn">
                <Row>
                  <Col
                    start={4}
                  >
                    <Button type="primary" htmlType="submit" className="login-form-button">提交</Button>
                    <Button className="login-form-button" onClick={this.handleCancel}>取消</Button>
                  </Col>
                </Row>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

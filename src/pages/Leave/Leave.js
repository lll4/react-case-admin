import React, { Component, Fragment } from 'react'
import { 
  Table, 
  Tag, 
  Spin, 
  Icon, 
  Button,
  Modal
} from 'antd';
import { 
  postLeavePagination, 
  postDeleteLeave
} from '@/requests';


export default class Leave extends Component {
  constructor() {
    super();
    // isSpinning是否为加载状态
    this.state = {
      testHtml: '<p>此操作不可逆！</p>',
      currentRecordId: null, //当前要删除哪一行的ID
      modalVisible: false,//是否显示删除对话框
      data: [],//列表里的数据
      isSpinning: true,//列表的加载状态
      confirmLoading: false,//确认删除的按钮是否显示loading
      // 默认在第一页，总页数为条数，每页条数为10。current当前页数
      pagination: {
        current: 1,//当前是第几页
        total: 0,//总共有多上条数据
        // pageSize: 10
      }
    }
  }
// 写在里面绑定到this上
//表格每列的信息
  columns = [{
    title: '编号',
    dataIndex: 'num',
    key: 'num',
    // render把指令传过来进行格式化
    render: text => <a>{text}</a>,
  }, {
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
  }, {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
  },{
    title: '职位',
    dataIndex: 'position',
    key: 'position',
    render: pos => {
      let color = "magenta"
      switch(pos) {
        case '科长': 
          color = '#f50';
          break;
        case '副科长':
          color = '#f5222d';
          break;
        case '处级领导':
          color = 'blue';
          break;
        case '科员':
          color = '#52c41a';
          break;
        case '队员':
          color = '#1890ff';
          break;
        case '治安科长':
          color = '#f50';
          break;
        default:
          break;
      }
      return <Tag color={color}>{pos}</Tag>
    }
  }, {
    title: '请假类型',
    key: 'cause',
    dataIndex: 'cause'
  }, {
    title: '请假原因',
    dataIndex: 'reason',
    key: 'reason',
  },{
    title: '请假时间',
    dataIndex: 'time',
    key: 'time',
  },{
    title: '操作',
    key: 'action',
    // record每一个的记录条数
    render: (text, record) => (
      <span>
        {/* Button.Group是两个按钮挨在一起 */}
        <Button.Group>
        <Button type="primary" onClick={this.toEdit.bind(this, record.id)} icon="edit" size="small">修改</Button>
        <Button
        onClick={this.handleDeleteButtonClick.bind(this, record.id)} 
        type="danger" 
        icon="delete" 
        size="small"
        >
        删除
        </Button>
        </Button.Group>
      </span>
    ),
  }];

  //点击删除按钮，显示模态框
  handleDeleteButtonClick = (id) => {
    // console.log(record)//record可以获取data里所有的东西
    this.setState({
      modalVisible: true,
      currentRecordId: id//获取id
    })
  }
  // 修改
  toEdit = (id) => {
    // console.log(id)
    // 跳转
    this.props.history.push(`/leave/editor/${id}`)
  }
// 录入
  toEntering = () => {
    this.props.history.push(`/leave/entering`)
  }
  //取消删除
    handleDeleteModal = () => {
      this.setState({
        modalVisible: false
      })
  }
  //确认删除
  // 点击之后返回true，最后又返回false
    handleDelete = () => {
      this.setState({
        confirmLoading: true
      }, () => {
      console.log(this.state.currentRecordId)
      postDeleteLeave(this.state.currentRecordId)
        .then(resp => {
          console.log(resp)
          // 从新刷新数据
          // this.fetchData()
          this.setState({
            confirmLoading: false,
            modalVisible: false,//modalVisible为false弹框隐藏
            isSpinning: true,
            // 删除之后返回第一页
            pagination: {
              ...this.state.pagination,
              current: 1
            }
          }, () => {
            this.fetchData();   
          })
        })
      })
    }
  
  // 响应结果,渲染假数据
    componentDidMount() {
      this.fetchData();
    }
  
    // handleTableChange点击改变,分页点击
    handleTableChange = (pagination) => {
      this.setState({
        pagination,
        isSpinning: true,
      }, () => {
       // 由于fetchData这个方法需要根据最新的stata来进行参数配置,所以这个时候需要在回调里去请求数据
        this.fetchData();
      })
    }
    fetchData() {
      // 使用当前页来计算从第几条开始
      const start = (this.state.pagination.current - 1) * 10;
      // 分页参数需要传入limited(每页的条数)，start从第几条开始
      postLeavePagination({
        limited: 10,//长度是多少条
        start//从第几条开始
      })
             .then(resp => {
              //  console.log(resp)
              this.setState({
                data:resp.list,
                isSpinning: false,
                // total: resp.totalCount//每次请求页数不一样
                pagination: {
                  // total: resp.totalCount,
                  pageSize: 10,
                  // current: resp.currentPage
                  current: this.state.pagination.current
                }
              })
             })
    }
    render() {
      return (
        <Fragment>
          <h2>病事假管理页面</h2>
          <Button onClick={this.toEntering} type="primary" style={{'marginLeft': '800px'}}>添加</Button>
          <Spin
          spinning={this.state.isSpinning}
           tip="加载中"
           indicator={<Icon type="loading" style={{ fontSize: 24 }} spin/>}
          >

          {/* pagination={{defaultPageSize: 30}}配置一页多少条 */}
          {/* rowKey必须绑定key */}
          <Table 
          rowKey={r => r.id}
          columns={this.columns} 
          dataSource={this.state.data}
          pagination={ this.state.pagination }
          onChange={this.handleTableChange}
          // total: 1000数据总数
          />
          </Spin>
          <Modal
            title="确认要删除吗？"
            visible={this.state.modalVisible}
            onOk={this.handleDelete}
            onCancel={this.handleDeleteModal}
            confirmLoading={this.state.confirmLoading}
            okText="确认"
            cancelText="取消"
          >
          {/* 在react里渲染html的方法 */}
            <div dangerouslySetInnerHTML={{__html: this.state.testHtml}}></div>
          </Modal>
        </Fragment>
      )
    }
  }
  
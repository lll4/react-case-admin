import React, { Component } from 'react'
import { Button } from 'antd';
import './retiremancheck.less'
import { 
  retiremancheck
} from '@/requests';

export default class Retiremancheck extends Component {
  constructor() {
    super();
    this.state = {
      num: '',
      name: '',
      age: '',
      position: '',
      address: '',
      phone: '',
      time: '',
      discription: ''
    }
  }

  componentDidMount () {
    const { id } = this.props.match.params
    retiremancheck(id).then(res => {
      console.log(res)
      this.setState({
        num: res.num,
        name: res.name,
        age: res.age,
        position: res.position,
        address: res.address,
        phone: res.phone,
        time: res.time,
        discription: res.discription
      })
    })
  }
  render() {
    return (
      <div className="wrap">
        <h1>离退信息</h1>
        <div className="num">
          <span>编号：</span>
          <span>{this.state.num}</span>
        </div>
        <div className="name">
          <span>姓名：</span>
          <span>{this.state.name}</span>
        </div>
        <div className="age">
          <span>年龄：</span>
          <span>{this.state.age}</span>
        </div>
        <div className="position">
          <span>职位：</span>
          <span>{this.state.position}</span>
        </div>
        <div className="address">
          <span>居住地址：</span>
          <span>{this.state.address}</span>
        </div>
        <div className="phone">
          <span>电话号码：</span>
          <span>{this.state.phone}</span>
        </div>
        <div className="time">
          <span>离退时间：</span>
          <span>{this.state.time}</span>
        </div>
        <div className="discription">
          <span>离退原因：</span>
          <span>{this.state.discription}</span>
        </div>
        <Button type="primary" className="btn">批准</Button>
        <Button type="primary" className="btn">不批准</Button>

      </div>
    )
  }
}

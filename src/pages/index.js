// import Dashboard from './Dashboard/Dashboard'
// import NotFound from './NotFound/NotFound'
// import Users from './Users/Users'
// import SysSetting from './SysSetting/SysSetting'

// react-loadable用于react页面级别组件的按需加载
import Loadable from 'react-loadable';

const Loading = () => null;

const Dashboard = Loadable({
  loader: () => import('./Dashboard/Dashboard'),
  loading: Loading,
});

const Users = Loadable({
  loader: () => import('./Users/Users'),
  loading: Loading,
});

const UserEdit = Loadable({
  loader: () => import('./Users/UserEdit'),
  loading: Loading,
});

const SysSetting = Loadable({
  loader: () => import('./SysSetting/SysSetting'),
  loading: Loading,
});

const NotFound = Loadable({
  loader: () => import('./NotFound/NotFound'),
  loading: Loading,
});

const CaseManage = Loadable({
  loader: () => import('./CaseManage/CaseManage'),
  loading: Loading,
});
// 登录
const Login = Loadable({
  loader: () => import('./Login/Login'),
  loading: Loading,
});
// 人员查看
const UsersView = Loadable({
  loader: () => import('./Users/UsersView'),
  loading: Loading,
});
// 人员详细信息
const UsersInfo = Loadable({
  loader: () => import('./Users/UsersInfo'),
  loading: Loading,
});
// 人员添加
const UsersAdd = Loadable({
  loader: () => import('./Users/UsersAdd'),
  loading: Loading,
});

const Leave = Loadable({
  loader: () => import('./Leave/Leave'),
  loading: Loading,
});

const Editor = Loadable({
  loader: () => import('./Leave/Editor'),
  loading: Loading,
});

const Entering = Loadable({
  loader: () => import('./Leave/Entering'),
  loading: Loading,
});

// 个人信息
const Person = Loadable({
  loader: () => import('./Person/Person'),
  loading: Loading,
});

// 个人信息修改
const PersonEditor = Loadable({
  loader: () => import('./Person/PersonEditor'),
  loading: Loading,
});

// 人员离退
const Retire = Loadable({
  loader: () => import('./Retire/Retire'),
  loading: Loading,
});


// 人员离退
const Retireman = Loadable({
  loader: () => import('./Retireman/Retireman'),
  loading: Loading,
});

// 人员离退详细信息
const Retiremancheck = Loadable({
  loader: () => import('./Retireman/Retiremancheck'),
  loading: Loading,
});

export {
  Dashboard,
  NotFound,
  Users,
  UserEdit,
  SysSetting,
  CaseManage,
  Login,
  UsersView,
  UsersInfo,
  UsersAdd,
  Leave,
  Editor,
  Entering,
  Person,
  PersonEditor,
  Retire,
  Retireman,
  Retiremancheck
}
import React, { Component} from 'react'
import {
  Form, 
  Input, 
  Button,
  Row,
  Col,
  Select,
  message,
  Upload,
  Icon
} from 'antd';
import './PersonEditor.less'

const { Option } = Select;
const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 12 },
}

//头像上传
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJPG = file.type === 'image/jpeg';
  if (!isJPG) {
    message.error('You can only upload JPG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJPG && isLt2M;
}

@Form.create()
export default class UsersAdd extends Component {

//头像上传
  state = {
    loading: false,
  };

  handleChange = (info) => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl => this.setState({
        imageUrl,
        loading: false,
      }));
    }
  }


  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }
  
  handleCancel = () => {
    this.props.history.push(`/`)
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">请上传照片</div>
      </div>
    );
    const imageUrl = this.state.imageUrl;
    return (
      <div className="wrap1">
      <h1 style={{'marginLeft': '400px'}}>个人信息修改</h1>
      <Row gutter={36} justify="center" type="flex">
       {/* justify="center" type="flex"居中 */}
        <Col span={24} start={13}>
      <Form onSubmit={this.handleSubmit} className="login-form">
      <Form.Item
         {...formItemLayout}
         className="Avatar"
      >
      {
            getFieldDecorator(
              'pic', 
            {
              rules: [{ 
                required: true, 
                message: '请传入照片'
              }],
            }
          )(
            <Upload
              name="avatar"
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={false}
              action="//jsonplaceholder.typicode.com/posts/"
              beforeUpload={beforeUpload}
              onChange={this.handleChange}
            >
              {imageUrl ? <img src={imageUrl} alt="avatar" /> : uploadButton}
          </Upload>
          )
        }
      
      </Form.Item>  
        <Form.Item
         {...formItemLayout}
         label="编号"
        >
          {
            getFieldDecorator(
              'num', 
            {
              rules: [{
                required: true,
                message: '请输入编号',
              }, {
                validator: (rule, value, cb) => {
                  var regex = /^[0-9]{5}$/; 
                if (value) {
                  if (regex.test(value)) { 
                    cb();
                  } else { 
                    cb('请输入五位数字');
                  }
                } else {
                  cb()
                }
              }
            }],
            }
          )(
            <Input
              placeholder="请输入编号"
            />
          )
        }
        </Form.Item>

          <Form.Item
         {...formItemLayout}
         label="姓名"
        >
          {
            getFieldDecorator(
              'name', 
            {
              // 判断条件
              // whitespace: true,检查空格
              rules: [{
                required: true,
                message: '请输入中文名'
              }, {
                validator: (rule, value, cb) => {
                  var regex = /^[\u4E00-\u9FA5]{2,4}$/; 
                if (value) {
                  if (regex.test(value)) {
                    cb();
                  } else {
                    cb('请输入中文名');
                  }
                } else {
                  cb()
                }
               }
              }],
            }
          )(
            <Input
              placeholder="请输入姓名"
            />
          )
        }
        </Form.Item>

          <Form.Item
         {...formItemLayout}
         label="年龄"
        >
          {
            getFieldDecorator(
              'age', 
            {
              // 判断条件
              // whitespace: true,检查空格
              rules: [{
                required: true,
                message: '请输入年龄',
              }, {
                validator: (rule, value, cb) => {
                  var regex = /^(1[9]|[2-8][0-9]|60)$/; 
                if (value) {
                  if (regex.test(value)) {
                    cb();
                  } else { 
                    cb('请输入年龄在19到60岁之间');
                  }
                } else {
                  cb()
                }
               }
              }],
            }
          )(
            <Input
              placeholder="请输入年龄"
            />
          )
        }
        </Form.Item>

    <Form.Item
          label="职位"
          {...formItemLayout}
        >
          {
            getFieldDecorator('position', {
            rules: [{ 
              required: true, 
              message: '请选择职位' 
            }],
          }
          )(
            <Select
              placeholder="请选择职位"
              onChange={this.handleSelectChange}
            >
              <Option value="科长">科长</Option>
              <Option value="副科长">副科长</Option>
              <Option value="科员">科员</Option>
              <Option value="队员">队员</Option>
              <Option value="处级领导">处级领导</Option>
              <Option value="治安科长">治安科长</Option>
            </Select>
          )}
        </Form.Item>

        
        
        <Form.Item
         {...formItemLayout}
         label="地址"
        >
          {
            getFieldDecorator(
              'address', 
            {
              rules: [{ 
                required: true, 
                message: '请输入地址' 
              }],
            }
          )(
            <Input
              placeholder="请输入地址"
            />
          )
        }
        </Form.Item>

        <Form.Item
          label="学历"
          {...formItemLayout}
        >
          {
            getFieldDecorator('education', {
            rules: [{ 
              required: true, 
              message: '请选择学历' 
            }],
          }
          )(
            <Select
              placeholder="请选择学历"
              onChange={this.handleSelectChange}
            >
              <Option value="初中">初中</Option>
              <Option value="高中">高中</Option>
              <Option value="大专">大专</Option>
              <Option value="本科">本科</Option>
            </Select>
          )}
        </Form.Item>

        <Form.Item
         {...formItemLayout}
         label="民族"
        >
          {
            getFieldDecorator(
              'nation', 
            {
              rules: [{ 
                required: true, 
                message: '请输入民族' 
              }],
            }
          )(
            <Input
              placeholder="请输入民族"
            />
          )
        }
        </Form.Item>

      
      <Form.Item
         {...formItemLayout}
         label="电话号码"
        >
          {
            getFieldDecorator(
              'phone', 
            {
              // 判断条件
              // whitespace: true,检查空格
              rules: [{
                required: true,
                message: '请输入电话号码',
              }, {
                validator: (rule, value, cb) => {
                  var regex = /^1[34578]\d{9}$/; 
                if (value) {
                  if (regex.test(value)) {
                    cb();
                  } else { 
                    cb('请输入电话号码');
                  }
                } else {
                  cb()
                }
               }
              }],
            }
          )(
            <Input
              placeholder="请输入电话号码"
            />
          )
        }
        </Form.Item>
        

        <Form.Item
         {...formItemLayout}
         label="个人简介"
        >
          {
            getFieldDecorator(
              'discription', 
            {
              rules: [{ 
                required: true, 
                message: '请输入个人简介' 
              }],
            }
          )(
            <textarea className="TextArea"></textarea>
          )
        }
        </Form.Item>

        <Form.Item className="btn">
          <Row>
            <Col
              start={4}
            >
              <Button type="primary" htmlType="submit" className="login-form-button">修改</Button>
              <Button className="login-form-button" onClick={this.handleCancel}>取消</Button>
            </Col>
          </Row>
        </Form.Item>
        
      </Form>
      </Col>
     </Row>
     </div>
    );
  }
}



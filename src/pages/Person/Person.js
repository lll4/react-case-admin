import React, { Component } from 'react'
import { Button } from 'antd';
import './person.less'
import { 
  person
} from '@/requests';

export default class Person extends Component {
  constructor() {
    super();
    this.state = {
      num: '',
      name: '',
      age: '',
      email: '',
      position: '',
      address: '',
      education: '',
      nation: '',
      phone: '',
      discription: '',
      img: ''
    }
  }

  componentDidMount () {
    person().then(res => {
      console.log(res.num)
      this.setState({
        num: res.num,
        name: res.name,
        age: res.age,
        position: res.position,
        email: res.email,
        address: res.address,
        education: res.education,
        nation: res.nation,
        phone: res.phone,
        discription: res.discription,
        img: res.img
      })
    })
  }
  // 修改\
  toEdit = () => {
    // 跳转
    this.props.history.push(`/person/personEditor`)
  }
  render() {
    return (
      <div className="wrap">
        <h1>个人信息</h1>
        <div className="img">
        <img src={this.state.img} alt=""/>
        </div>
        <div className="num">
          <span>编号：</span>
          <span>{this.state.num}</span>
        </div>
        <div className="name">
          <span>姓名：</span>
          <span>{this.state.name}</span>
        </div>
        <div className="num">
          <span>年龄：</span>
          <span>{this.state.num}</span>
        </div>
        <div className="position">
          <span>职位：</span>
          <span>{this.state.position}</span>
        </div>
        <div className="email">
          <span>邮箱：</span>
          <span>{this.state.email}</span>
        </div>
        <div className="address">
          <span>地址：</span>
          <span>{this.state.address}</span>
        </div>
        <div className="education">
          <span>学历：</span>
          <span>{this.state.education}</span>
        </div>
        <div className="nation">
          <span>民族：</span>
          <span>{this.state.nation}</span>
        </div>
        <div className="phone">
          <span>电话号码：</span>
          <span>{this.state.phone}</span>
        </div>
        <div className="discription">
          <span>个人简介：</span>
          <span>{this.state.discription}</span>
        </div>
        <Button onClick={this.toEdit} type="primary" className="btn">修改</Button>
      </div>
    )
  }
}

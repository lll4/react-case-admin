import React, { Component } from 'react'

import {
  Form, Input, Col, Avatar, Spin, Icon
} from 'antd';

import "./UserInfo.less";
import { postUserInfo } from "../../requests"


@Form.create()
export default class UserInfo extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      address: '',
      age: '',
      position: '',
      gender: '',
      email: '',
      isSpinning: true
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }
  componentDidMount () {
    const { id } = this.props.match.params
    postUserInfo(id).then(res => {
      this.setState({
        name: res.name,
        address: res.address,
        age: res.age,
        position: res.position,
        gender: res.gender,
        email: res.email,
        isSpinning: false
      })
    })

  }

  render () {

    const formItemLayout = {
      labelCol: {
        sm: { span: 8 },
      },
      wrapperCol: {
        sm: { span: 8 },
      },
    };
    return (
      <Spin
        spinning={this.state.isSpinning}
        tip="加载中..."
        indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
      >
        <div className="UserInfo-warpper">
          <Col span={16} offset={2}>
            <Avatar size={64} icon="user" />
            <Form
              onSubmit={this.handleSubmit}
              className="login-form">
              <Form.Item
                label="姓名"
                {...formItemLayout}
              >
                {(
                  <Input type="text" disabled={true} value={this.state.name} />
                )}
              </Form.Item>

              <Form.Item
                label="性别"
                {...formItemLayout}
              >
                {(
                  <Input type="text" disabled={true} value={this.state.gender} />
                )}
              </Form.Item>

              <Form.Item
                label="年龄"
                {...formItemLayout}
              >
                {(
                  <Input type="text" disabled={true} value={this.state.age} />
                )}
              </Form.Item>

              <Form.Item
                label="职位"
                {...formItemLayout}
              >
                {(
                  <Input type="text" disabled={true} value={this.state.position} />
                )}
              </Form.Item>

              <Form.Item
                label="性别"
                {...formItemLayout}
              >
                {(
                  <Input type="text" disabled={true} value={this.state.gender} />
                )}
              </Form.Item>

              <Form.Item
                label="地址"
                {...formItemLayout}
              >
                {(
                  <Input type="text" disabled={true} value={this.state.address} />
                )}
              </Form.Item>

              <Form.Item
                label="邮箱"
                {...formItemLayout}
              >
                {(
                  <Input type="text" disabled={true} value={this.state.email} />
                )}
              </Form.Item>

            </Form>
          </Col>
        </div>
      </Spin>
    );
  }
}

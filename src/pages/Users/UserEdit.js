import React, { Component, createRef } from 'react'
import {
  Row,
  Col,
  Form,
  Input,
  Button,
  Radio
} from 'antd';
import E from 'wangeditor';

const formItemLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 16 },
}

@Form.create()
export default class UserEdit extends Component {
  constructor() {
    super();
    this.editorDom = createRef();
    this.state = {
      descHtml: '<h1>你好！</h1>'
    }
  }
  componentDidMount() {
    // 这个有可能需要在ajax完成初始数据的时候去做。
    this.initWangeditor()
  }
  initWangeditor() {
    // 初始化一个editor
    this.editor = new E(this.editorDom.current)
    // 当editor的内容改变的时候，需要去setState，以确保最后编辑的内容能提交
    this.editor.customConfig.onchange = (descHtml) => {
        // html 即变化之后的内容
        this.setState({
          descHtml
        })
    }
    // 创建编辑器
    this.editor.create();
    // 设置初始值
    this.editor.txt.html(this.state.descHtml)
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }
  normFile = (e) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Form.Item
          {...formItemLayout}
          label="姓名"
        >
          {
            getFieldDecorator(
              'userName',
              {
                rules: [{
                  required: true,
                  whitespace: true,
                  message: '请输入姓名'
                }, {
                  max: 16,
                  message: '姓名最多16位'
                }, {
                  min: 2,
                  message: '姓名最少2位'
                }],
              }
            )(
              <Input
                placeholder="请输入姓名"
              />
            )
          }
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="年龄"
        >
          {
            getFieldDecorator(
              'age',
              {
                rules: [{
                  required: true,
                  validator: (rule, value, cb) => {
                    const v = parseInt(value, 10);
                    if (!v) {
                      cb('请输入数字')
                    } else if (v < 18 || v > 55 || Number(value) !== v) {
                      cb('工作年龄应该是在18-55岁的正整数')
                    } else {
                      cb()
                    }
                  }
                }],
              }
            )(
              <Input
                placeholder="请输入年龄"
              />
            )
          }
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="性别"
        >
          {getFieldDecorator(
            'gender', {
              initialValue: 'F',
            }
          )(
            <Radio.Group>
              <Radio value="M">男</Radio>
              <Radio value="F">女</Radio>
            </Radio.Group>
          )}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="图文简介"
        >
          {getFieldDecorator(
            'desc', {
              initialValue: this.state.descHtml,
            }
          )(
            <div ref={this.editorDom}></div>
          )}
        </Form.Item>
        <Form.Item>
          <Row>
            <Col
              offset={4}
            >
              <Button type="primary" htmlType="submit" className="login-form-button">修改</Button>
              <Button className="login-form-button">取消</Button>
            </Col>
          </Row>
        </Form.Item>
      </Form>
    );
  }
}

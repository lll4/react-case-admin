import React, { Component, Fragment } from 'react'
import {
  Table,
  Tag,
  Spin,
  Icon,
  Button,
  Modal,
  Tooltip
} from 'antd';

// 这里引入的是ajax的请求方法
import {
  postUserListPagination,
  postDeleteUser
} from '@/requests';

export default class Users extends Component {
  constructor() {
    super();
    this.state = {
      testHtml: '<p>此操作不可逆！！！</p>',
      currentRecordId: null, // 当前要删除哪一行的ID
      modalVisible: false, // 是否显示删除对话框
      data: [], // 列表里的数据
      isSpinning: true, // 列表的加载状态
      confirmLoading: false, // 确定删除的按钮是否显示loading
      pagination: {
        current: 1, // 当前是第几页
        total: 0 // 总有多少条数据
      }
    }
  }

  // 表格每列的信息: 手册上有
  columns = [{
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
    render: text => text,
  }, {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
  }, {
    title: '性别',
    dataIndex: 'gender',
    key: 'gender',
  }, {
    title: '地址',
    dataIndex: 'address',
    key: 'address',
  }, {
    title: '职位',
    key: 'position',
    dataIndex: 'position',
    render: pos => {
      let color = "lime"
      switch(pos) {
        case '管理员': 
          color = 'cyan';
          break;
        case '普通用户':
          color = 'blue';
          break;
        case '经理':
          color = '#f50';
          break;
        default:
          break;
      }
      return <Tag color={color}>{pos}</Tag>
    }
  }, {
    title: '操作',
    key: 'action',
    render: (text, record) => (
      <span>
        <Button.Group>
          <Tooltip placement="top" title={`点击编辑 ${record.name}`}>
            <Button onClick={this.toEdit.bind(this, record.id)} type="primary" icon="edit" size="small">编辑</Button>
          </Tooltip>
          <Tooltip placement="top" title={`点击删除 ${record.name}`}>
            <Button
              onClick={this.hanleDeleteButtonClick.bind(this, record.id)}
              type="danger"
              icon="delete"
              size="small"
            >
            删除
            </Button>
          </Tooltip>
        </Button.Group>
      </span>
    ),
  }]

  toEdit = (id) => {
    this.props.history.push(`/users/edit_user/${id}`)
  }
  // 点击删除按钮，显示模态框
  hanleDeleteButtonClick = (id) => {
    this.setState({
      modalVisible: true,
      currentRecordId: id
    })
  }

  // 取消删除
  hideDeleteModal = () => {
    this.setState({
      modalVisible: false
    })
  }

  // 确认删除
  handleDelete = () => {
    
    this.setState({
      confirmLoading: true // 显示确认按钮上的loading
    }, () => {
      postDeleteUser(this.state.currentRecordId) // 请求删除接口
        .then(resp => {
          this.setState({
            confirmLoading: false, // 隐藏确认按钮上的loading
            modalVisible: false, // 隐藏模态框
            isSpinning: true, // 显示表格上的spin
            pagination: {
              ...this.state.pagination,
              current: 1 // 回到第一页
            }
          }, () => {
            this.fetchData();  // 再次重新请求数据，这样才能确保你删除之后的列表数据为最新
          })
        })
    })
  }

  componentDidMount() {
    this.fetchData();
  }
  // 分页点击
  handleTableChange = (pagination) => {
    this.setState({
      pagination,
      isSpinning: true,
    }, () => {
      // 由于fetchData这个方法需要根据最新的state来进行参数配置，所以这个时候需要在回调里去请求数据
      this.fetchData();
    })
  }
  // 请求数据
  fetchData() {
    // 根据当前页，计算出从第几条数据开始
    const offset = (this.state.pagination.current - 1) * 10;
    // 分页参数需要传入limited(每页的条数), offset是从第几条开始
    postUserListPagination({
      limited: 10,
      offset
    })
      .then(resp => {
        this.setState({
          data: resp.list,
          isSpinning: false,
          pagination: {
            total: resp.totalCount,
            current: this.state.pagination.current // current: resp.currentPage
          }
        })
      })
  }

  render() {
    return (
      <Fragment>
        <h2>
          人员管理
        </h2>
        <Spin
          spinning={this.state.isSpinning}
          tip="加载中..."
          indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
        >
          <Table
            rowKey={r => r.id}
            columns={this.columns}
            dataSource={this.state.data}
            pagination={this.state.pagination}
            onChange={this.handleTableChange}
          />
        </Spin>
        <Modal
          title="确认要删除吗？"
          visible={this.state.modalVisible}
          onOk={this.handleDelete}
          onCancel={this.hideDeleteModal}
          confirmLoading={this.state.confirmLoading}
          okText="确认"
          cancelText="取消"
        >
          {/* 在react里要渲染html的方法 */}
          <div dangerouslySetInnerHTML={{__html: this.state.testHtml}}></div>
        </Modal>
      </Fragment>
    )
  }
}

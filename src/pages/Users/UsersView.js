import React, { Component } from 'react'
import { Table, Tag, Button, Spin, Icon, Input } from 'antd';

import { postUsersViewList, postSearch } from "../../requests";
import "./UsersView.less"

const Search = Input.Search;

export default class UsersView extends Component {
  constructor() {
    super();
    this.state = {
      isspinning: true,
      data: [],
      pagination: {
        current: 1, //当前第几页
        total: 0,//总共有几条数据
        hideOnSinglePage: true // 只有一页时是否隐藏分页器
      }
    }
  }
  columns = [{
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
    render: text => <span>{text}</span>,
  }, {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
  }, {
    title: '地址',
    dataIndex: 'address',
    key: 'address',
  }, {
    title: '职位',
    key: 'position',
    dataIndex: 'position',
    render: position => {
      let color = "magenta"
      switch (position) {
        case "科长":
          color = "volcano";
          break;
        case "副科长":
          color = "red";
          break;
        case "处级领导":
          color = "orange";
          break;
        case "科员":
          color = "geekblue";
          break;
        default:
          break;
      }
      return <Tag color={color} >{position}</Tag>

    }
  }, {
    title: '操作',
    key: 'action',
    render: (text, record) => (
      <span>
        {/* 查看按钮 */}
        <Button
          type="primary"
          size="small"
          onClick={this.handleLookBtnClick.bind(this, record.id)}
        >查看</Button>
      </span>
    )
  }];
  // 执行这个生命周期的时候请求数据
  componentDidMount () {
    this.fetchData();
  }
  // 请求数据函数
  fetchData = () => {
    // 计算从第几条开始
    const offset = (this.state.pagination.current - 1) * 10;
    postUsersViewList({
      //每页的限制条数
      limited: 10,
      // 从第几条开始
      offset
    }).then(resp => {
      this.setState({
        isspinning: false,
        data: resp.list,
        pagination: {
          //总数量
          total: resp.totalCount,
          //当前页数
          current: this.state.pagination.current  //current: resp.currentPage
        }
      })
    })
  }
  // 分页点击
  handlePagenationChange = (pagination) => {
    // 这个pagenation参数是antd组件提供的，可以打印出来看就知道了
    this.setState({
      pagination,
      isspinning: true
    }, () => {
      this.fetchData();
    })
  }
  // 点击查看按钮
  handleLookBtnClick = (id) => {
    this.props.history.push(`/users_view/user_info/${id}`)
  }
  // 点击搜索按钮
  searchBtnClick = (value, e) => {
    // 点击搜索后，出现加载中的状态
    this.setState({
      isspinning: true
    })
    // 如果输入框没有东西，就默认显示所有的人员的列表
    if (value === "") {
      this.fetchData();
    } else {
      // 如果输入了东西，就调用查询接口
      postSearch({
        name: value
      }).then(resp => {
        // 接口返回来数据，赋值给data,页面渲染，把加载状态修改为false
        this.setState({
          data: resp.list,
          isspinning: false,
          pagination: {
            total: resp.totalCount
          }
        })
      })
    }

  }
  render () {
    return (
      <Spin
        spinning={this.state.isspinning}
        tip="加载中..."
        indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />}
      >
        {/* 搜索框 */}
        <Search
          className="searchInput"
          placeholder="请输入搜索的人员"
          enterButton="搜索"
          size="default"
          onSearch={this.searchBtnClick}
        />
        <Table
          rowKey={record => record.id}
          columns={this.columns}
          dataSource={this.state.data}
          pagination={this.state.pagination}
          onChange={this.handlePagenationChange}
          single
        />
      </Spin>
    )
  }
}

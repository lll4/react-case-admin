import React, { Component, Fragment } from 'react'
import {
  Form, Icon, Input, Button
} from 'antd';

import { Redirect } from 'react-router-dom'
import "./login.less"
import { doLogin } from "../../actions/login"
import { connect } from "react-redux"


// 从state里面取出isLoading和isLogin放到this.props里面
const mapState = (state) => {
  return {
    isLoading: state.users.isLoading,
    isLogin: state.users.isLogin
  }
}


// 这三个有顺序问题，只能这样排序
// react-redux提供的高阶组件，第一个参数是state，第二个参数是action
@connect(mapState, { doLogin })
// antd form表单高阶组件Form.create()
@Form.create()
export default class Login extends Component {
  // state = {
  //   isLogin: false
  // }
  // static getDerivedStateFromProps(props) {
  //   console.log(props.isLogin)
  //   return {
  //     isLogin: props.isLogin
  //   }
  // }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form:', values);
        //如果输入成功，执行登录
        // 调用action里面的登录执行函数
        this.props.doLogin(values)
      }
    });
  }

  render () {
    // console.log(this.props)
    const { getFieldDecorator } = this.props.form;
    const { isLoading, isLogin, history } = this.props
    const {
      from = './dashboard'
    } = history.location.state
    // console.log(from)
    // 页面跳转
    return isLogin
      ? <Redirect to={from}/>
      : (
        <Fragment>
        <div className="top_div"></div>
        <div className="login">
        <div className="tou_pic">
        <div className="wrap3">
          <div className="tou"></div>
          <div className="left"></div>
          <div className="right"></div>
        </div>
        </div>
          <Form onSubmit={this.handleSubmit} className="login-form">
            <Form.Item>
              {getFieldDecorator('username', {
                rules: [{ required: true, message: '请输入用户名' }],
              })(
                <Input disabled={isLoading} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="请输入用户名" />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{
                  required: true,
                  message: '请输入密码'
                }, {
                  min: 6,
                  message: '密码不能少于6位'
                },],
              })(
                <Input disabled={isLoading} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码" />
              )}
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                loading={isLoading}
              >
                登录
            </Button>
            </Form.Item>
          </Form>
        </div>
        </Fragment>
      );
  }
}
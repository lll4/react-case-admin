import {
  Dashboard,
  NotFound,
  UsersAdd,
  UsersInfo,
  SysSetting,
  CaseManage,
  UsersView,
  Leave,
  Editor,
  Entering,
  Person,
  PersonEditor,
  Retire,
  Retireman,
  Retiremancheck
} from './pages'

const routes = [{
  path: '/dashboard',
  component: Dashboard,
  iconType: "dashboard",
  permission: ["副科长","治安科长","处级领导","科员","队员"],
  // isAuthRequired: true,
  isNav: true,
  title: '首页'
}, {
  path: '/users',
  iconType: "user",
  isNav: true,
  isExact: true,
  permission: ["副科长","治安科长","处级领导","科员","队员"],
  title: '人员管理',
  children: [{
    path: '/leave',
    isExact: true,
    isNav: true,
    component: Leave,
    iconType: "profile",
    permission: ["副科长","治安科长"],
    title: '病事假管理',
    havechildren: true
  }, {
    path: '/users_view',
    iconType: "search",
    component: UsersView,
    permission: ["副科长","治安科长","处级领导"],
    isNav: true,
    isExact: true,
    title: '人员查看'
  }, {
    path: '/users_add',
    iconType: "user-add",
    permission: ["副科长","治安科长"],
    component: UsersAdd,
    isNav: true,
    isExact: true,
    title: '人员添加'
  }, {
    path: '/person',
    iconType: "info",
    component: Person,
    isNav: true,
    isExact: true,
    permission: ["科员","队员"],
    title: '个人信息'
  }, {
    path: '/retire',
    iconType: "setting",
    component: Retire,
    isNav: true,
    isExact: true,
    permission: ["副科长","治安科长","处级领导","科员","队员"],
    title: '离退申请'
  }, {
    path: '/retireman',
    iconType: "setting",
    component: Retireman,
    isNav: true,
    isExact: true,
    permission: ["副科长","治安科长"],
    title: '离退人员查看'
  }]
}, {
  path: '/users_view/user_info/:id',
  component: UsersInfo,
  isExact: true,
}, {
  path: '/case_manage',
  isExact: true,
  isNav: true,
  component: CaseManage,
  iconType: "profile",
  permission: ["副科长","治安科长","处级领导","科员","队员"],
  title: '案事件流程管理',
  havechildren: true
}, {
  path: '/leave/editor/:id',
  isExact: true,
  component: Editor,
  title: '编辑病事假管理'
}, {
  path: '/leave/entering',
  isExact: true,
  component: Entering,
  title: '添加病事假管理'
},{
  path: '/person/personEditor',
  isExact: true,
  component: PersonEditor,
  title: '编辑病事假管理'
}, {
  path: '/syssetting',
  component: SysSetting,
  iconType: "setting",
  isNav: true,
  permission: ["副科长","治安科长","科员","队员"],
  title: '加班管理',
  havechildren: true
},{
  path: '/retireman/retiremancheck/:id',
  isExact: true,
  component: Retiremancheck,
  title: '编辑病事假管理'
}, {
  path: '/404',
  component: NotFound
}]

export default routes

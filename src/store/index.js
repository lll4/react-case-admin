// 需要使用createStore来创建一个store
import { createStore, applyMiddleware } from "redux"
// 用于异步处理action
import thunk from "redux-thunk"

import rootReducer from "../reducers"
// 这里的第一个参数必须是reducer,reducer是一个方法
export default createStore(rootReducer, applyMiddleware(thunk))